### MAC0350 Introdução ao Desenvolvimento de Sistemas de Software
# Projeto Parte 2

# Script para criar dml sql que povoa banco de dados com as relações person e personfriend 

# Por
# Arthur Pilone Maia da Silva, NUSP 11795450
# e Lorenzo Bertin Salvador, NUSP 11795356

import csv
import random
import math 

random.seed(a=1)
qtd = 600

def testaIDS(lista):
	idx = 1
	for i in lista:
		if int(i[0]) != idx:
			print("Erro",i[0],idx)
		idx += 1

def imprimeInsercaoIndividuos(lista):
	print('--- Pessoas ---')
	for i in lista:
		ID = i[0]
		nome = i[1]
		print("INSERT INTO PERSON VALUES (",str(ID),",","'",nome,"');",sep='')

def relacaoValida(ID,a,b):

	# Apenas um dos dois valores tem que ser = ID, e o outro não pode ser ID
	if a == ID and b == ID:
		return False
	elif a != ID and b != ID:
		return False
	elif a == ID and ID == 1 and b == 2:
		return False
	elif a == ID and ID == 2 and b == 1:
		return False
	elif b == ID and ID == 1 and a == 2:
		return False
	elif b == ID and ID == 2 and a == 1:
		return False
	return True


def criaRelacoesBobAlice(lista,relacoes):
	# Bob
	ID = 1
	a = -1
	b = -1
	while ID < 3:
		for i in range(0,500):
			while relacaoValida(ID,a,b) == False or [a,b] in relacoes:
				if random.randint(1,2) == 1:
					a = ID
					b = random.randint(1,qtd)
				else:
					a = random.randint(1,qtd)
					b = ID
			dupla = [a,b]
			relacoes.append(dupla)
		# Alice
		ID += 1
	return relacoes

'''
	Recebe uma lista de IDs válidos e cria relações entre eles
'''

def seleciona(possiveis,relacoes):
	usadas = []
	a = -1
	b = -1
	for i in range(0,math.floor(len(possiveis)/2)):
		while a == b or a not in possiveis or b not in possiveis or [a,b] in relacoes:
			a = random.randint(3,qtd)
			b = random.randint(3,qtd)
		dupla = [a,b]
		if a not in usadas:
			usadas.append(a)
		if b not in usadas:
			usadas.append(b)
		relacoes.append(dupla)
	possiveis2 = []
	for i in possiveis:
		if i not in usadas:
			possiveis2.append(i)

	# Possiveis 2 é a lista de IDs ainda não utilizados
	return [possiveis2,relacoes]

def criaRelacoesAleatorias(lista,relacoes):

	# possiveis = lista de IDs amigos de bob e/ou alice
	possiveis = []
	for i in relacoes:
		if i[0] != 1 and i[0] != 2:
			continue
		if i[1] not in possiveis:
			possiveis.append(i[1])


	possiveis2, relacoes = seleciona(possiveis,relacoes)

	relacoes = ordenaRelacoes(relacoes)

	possiveis3, relacoes = seleciona(possiveis2,relacoes)

	relacoes = ordenaRelacoes(relacoes)

	possiveis3, relacoes = seleciona(possiveis3, relacoes)

	relacoes = ordenaRelacoes(relacoes)

	return relacoes

def imprimeRelacoes(lista,relacoes):
	print('--- Amizades---')
	a0 = -1
	anterior = 1
	for i in relacoes:
		a = i[0]
		b = i[1]
		if a != a0:
			print("\n-- Amigos de",lista[a-1][1])
			a0 = a
		print("INSERT INTO PERSONFRIEND VALUES ","(",a,",",b,");",sep='')

def ordenaRelacoes(relacoes):
	relacoes.sort()
	return relacoes

def testaRelacoes(relacoes):
	qtdBobEAlice = 0
	for i in relacoes:
		if i[0] == 1 or i[0] == 2 or i[1] == 1 or i[1] == 2:
			qtdBobEAlice +=1

	if qtdBobEAlice != 1000:
		print("Erro no bob e alice",qtdBobEAlice)
		quit()


def main():
	
	with open('nomes.csv', newline='') as f:
		reader = csv.reader(f)
		lista = list(reader)
	lista = lista[1:qtd+2]
	testaIDS(lista)

	
	relacoes = []
	relacoes = criaRelacoesBobAlice(lista,relacoes)
	relacoes = ordenaRelacoes(relacoes)



	relacoes = criaRelacoesAleatorias(lista,relacoes)

	relacoes = ordenaRelacoes(relacoes)

	testaRelacoes(relacoes)

	imprimeInsercaoIndividuos(lista)
	imprimeRelacoes(lista,relacoes)

main()