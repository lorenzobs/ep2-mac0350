### Julho de 2022
### MAC0350 Introdução ao Desenvolvimento de Sistemas de Software
# Projeto Parte 2
## Por
### Arthur Pilone Maia da Silva, NUSP 11795450
### e Lorenzo Bertin Salvador, NUSP 11795356

Entre os arquivos de nossa entrega, temos:

- `Relatorio.pdf`: Uma descrição do que fizemos e algumas das escolhas que fizemos. Recomenda-se ler o relatório como um _roteiro_.

**Arquivos do Item 1:** 

- `DDLprojeto.sql`: Script em SQL DDL para a definição das relações utilizadas no banco.
- `DMLpopulaGeral.sql`: Script em SQL DML utilizado para povoar o banco de dados. 
- `script.py`: Script em _python_ utilizado para gerar o DML.

**Arquivos do Item 2:** 

- `consultasItem2.sql`: Script SQL com as consultas realizadas para o banco relacional (consultas 2.1 a 2.3).

**Arquivos do Item 3:** 

- `exportToNEO4J.txt`: Compilado de comandos utilizados para exportar o arquivo `person.csv` a  partir do banco relacional e para importar as relações para o sistema gerenciador do _Neo4J_.

**Arquivos do Item 4:** 

- `consultaItem4.cql`: Consulta do Item 4 escrita em _Cypher_.

**Arquivos do Item 5:**

Não foram gerados arquivos separados para o Item 5, sendo os métodos utilizados para os testes e os resultados dos testes em si encontrados no **relatório**.